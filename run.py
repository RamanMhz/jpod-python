import tkinter as tk
from tkinter import scrolledtext as st
from tkinter import constants as const

from pathlib import Path
import os
import json


font_Monotype_Corsiva = "-family {Monotype Corsiva} -size 24 -weight bold " + "-slant italic"
font_Comic_Sans_MS = "-family {Comic Sans MS} -size 14"
font_Comic_Sans_MS_16 = "-family {Comic Sans MS} -size 16"
font_Segoe_UI_16 = "-family {Segoe UI} -size 16 -weight bold"

class ScrollFrame():
	def __init__(self, masterFrame):
		self.canvas=tk.Canvas(masterFrame)
		self.frame=tk.Frame(self.canvas, height=720, width=480)
		myscrollbarv=tk.Scrollbar(masterFrame,orient="vertical",command=self.canvas.yview)
		myscrollbarh=tk.Scrollbar(masterFrame,orient="horizontal",command=self.canvas.xview)
		self.canvas.configure(yscrollcommand=myscrollbarv.set)
		self.canvas.configure(xscrollcommand=myscrollbarh.set)

		myscrollbarv.pack(side="right",fill="y")
		myscrollbarh.pack(side="bottom",fill="x")
		self.canvas.pack(fill="x")
		self.canvas.create_window((0,0),window=self.frame,anchor='nw')
		self.frame.bind("<Configure>", self.myfunction)
		self.frame.bind('<Enter>', self.bound_to_mousewheel)
		self.frame.bind('<Leave>', self.unbound_to_mousewheel)

	def bound_to_mousewheel(self, event):
		self.canvas.bind_all("<MouseWheel>", self.on_mousewheel)

	def unbound_to_mousewheel(self, event):
		self.canvas.unbind_all("<MouseWheel>") 

	def on_mousewheel(self, event):
		self.canvas.yview_scroll(int(-1*(event.delta/120)), "units")

	def myfunction(self, event):
	    # canvas.configure(scrollregion=canvas.bbox("all"), height=720, width=480)
		size = (self.frame.winfo_reqwidth(), self.frame.winfo_reqheight())
		self.canvas.config(scrollregion='0 0 %s %s' % size)
		if self.frame.winfo_reqwidth() != self.canvas.winfo_width():
			self.canvas.config(width = self.frame.winfo_reqwidth())
		if self.frame.winfo_reqheight() != self.canvas.winfo_height():
			self.canvas.config(height = self.frame.winfo_reqheight())



def changeFrame(currentFrame, nextFrame):
	# currentFrame.destroy()
	currentFrame.pack_forget()
	nextFrame.pack(fill="both", expand=1)

def UI():
	root = tk.Tk()
	root.title("Innovative Language")
	root.geometry("480x720+350+25")
	root.resizable(1, 1)
	root.minsize(500, 500)
	root.config(bg = "#d9d9d9")
	rootFrame = tk.Frame(root, height=720, width=480)
	rootFrame.pack(fill="both", expand=1)

	global currentFrame
	currentFrame = rootFrame

	categoryBtn = tk.Button(rootFrame,
		text = 'Category',
		bg  = "#ff7735",
		font = font_Comic_Sans_MS,
		anchor = 'w',
		padx = 12,
		command = lambda: changeFrame(rootFrame, categoryFrame)).pack(
		anchor = 'ne',
		padx = 12,
		pady = 6,
		fill = 'x')
	pdfBtn = tk.Button(rootFrame,
		text = 'PDFs',
		bg  = "#ff7735",
		font = font_Comic_Sans_MS,
		anchor = 'w',
		padx = 12).pack(
		anchor = 'ne',
		padx = 12,
		fill = 'x')

	categoryFrame = tk.Frame(root, height=720, width=480)
	scrollableCatFrame = ScrollFrame(categoryFrame).frame

	tk.Label(scrollableCatFrame,
	text = "Category",
	font = font_Monotype_Corsiva,
	padx = 6,
	pady = 2,
	bg = "#c4ff0e"
	).pack(fill='x')

	with open('currentDetails.txt', 'r', encoding='utf-8') as fp:
		currentDetails = json.load(fp)
	categoryList = [x for x in currentDetails]
	# print(categoryList)
	for i, catNum in enumerate(categoryList):
		text = multilinetext(currentDetails[catNum]['title'], 49) 
		selectCatBtn = tk.Button(scrollableCatFrame,
		text = text,
		bg  = "#ff7735",
		font = font_Comic_Sans_MS,
		# anchor = 'w',
		padx = 12,
		width=39,
		command = lambda x=catNum: showLessonsList(root, categoryFrame, currentDetails, x)
		)
		selectCatBtn.pack(
		# anchor = 'ne',
		padx = 9,
		pady = 6)
	backBtn = tk.Button(scrollableCatFrame,
	text = 'Back',
	bg  = "#ff7735",
	font = font_Comic_Sans_MS,
	anchor = 'w',
	padx = 12,
	command = lambda: changeFrame(categoryFrame, rootFrame)).pack(
	anchor = 'se',
	padx = 36,
	pady = 36,
	side = tk.BOTTOM)

	root.mainloop()

def showLessonsList(root, categoryFrame, currentDetails, catNum):

	lessonsListFrame = tk.Frame(root, 
		#relief=tk.GROOVE,
		 bd=1, height=720, width=480)
	changeFrame(categoryFrame, lessonsListFrame)

	scrollableFrame = ScrollFrame(lessonsListFrame).frame

	title = multilinetext(currentDetails[catNum]['title'], 35)

	tk.Label(scrollableFrame,
	text = title,
	font = font_Monotype_Corsiva,
	padx = 6,
	pady = 2,
	bg = "#c4ff0e",
	anchor = 'w',
	justify = 'left'
	).pack(fill='x')
	
	for i, lesson in enumerate(currentDetails[catNum]['lessons']):
		lessonPath = currentDetails[catNum]['lessons'][lesson]["Path"]
		text = multilinetext(currentDetails[catNum]['lessons'][lesson]["Des"], 49)
		selectlessonBtn = tk.Button(scrollableFrame,
		text = text,
		bg  = "#ffca18",
		font = font_Comic_Sans_MS,
		anchor = 'w',
		justify = 'left',
		padx = 12,
		width = 40,
		command = lambda lessonPath=lessonPath: openLesson(root, lessonsListFrame, lessonPath)).pack(
		pady = 2,
		fill = 'x')

	backBtn = tk.Button(scrollableFrame,
		text = 'Back',
		bg  = "#ff7735",
		font = font_Comic_Sans_MS,
		padx = 12,
		# command = lambda: changeFrame(lessonsListFrame, categoryFrame)).grid(
		# row=i*4, column=1, sticky=tk.E)
		command = lambda: changeFrame(lessonsListFrame, categoryFrame)).pack(
			anchor = 's',
			padx = 36,
			pady = 36,
			# side = tk.LEFT
			)

def openLesson(root, lessonsListFrame, lessonPath):
	lessonFrame = tk.Frame(root, height=720, width=480)
	changeFrame(lessonsListFrame, lessonFrame)

	with open(os.path.join(lessonPath, 'lessonDetails.txt'), 'r', encoding='utf-8') as jf:
		lessonDetails = json.load(jf)

	# with open('lessonDetails.json', 'w', encoding='utf-8') as jf:
	# 	json.dump(lessonDetails, jf, ensure_ascii=False, indent=4)

	# Descrip = lessonDetails["data"]["Lesson"]["Description"].split(" ")
	# finalDes = ""
	# linewordcount = 0
	# for i, word in enumerate(Descrip):
	# 	linewordcount += len(word) + 1
	# 	joiner = "" if i is 0 else " " if linewordcount < 52 else "\n"
	# 	if linewordcount >= 52:
	# 		linewordcount = 0
	# 	finalDes += joiner + word 
	title = multilinetext(lessonDetails["data"]["Lesson"]["Title"], 48)

	tk.Label(lessonFrame,
		text = title,
		font = font_Comic_Sans_MS_16,
		padx = 6,
		pady = 2,
		bg = "#c4ff0e"
		).pack(fill='x')

	medialist = lessonDetails["data"]["Lesson"]["AudioFiles"]
	for x in lessonDetails["data"]["Lesson"]["VideoFiles"]: medialist.append(x)

	for mediafile in medialist:
		# url = audio_file["Url"]
		# path_parts = url.split("/")
		# path = Path("/".join(path_parts[len(path_parts)-3:]))
		path = os.path.join(lessonPath, Path(mediafile["Url"]).name)
		tk.Button(lessonFrame,
		text = mediafile["Type"],
		bg  = "#ffca18",
		font = font_Comic_Sans_MS,
		padx = 12,
		command = lambda path=path: os.startfile(path)).pack(
		pady = 2,
		fill = 'x')

	if lessonDetails["data"]["Lesson"].__contains__("Transcript"):
		tk.Button(lessonFrame,
		text = 'Transcript',
		bg  = "#ffca18",
		font = font_Comic_Sans_MS,
		padx = 12,
		command = lambda: openTranscriptFrame(root, lessonFrame, lessonDetails, lessonPath)).pack(
		pady = 2,
		fill = 'x')

	if lessonDetails["data"]["Lesson"].__contains__("Expansion"):
		tk.Button(lessonFrame,
		text = 'Expansion',
		bg  = "#ffca18",
		font = font_Comic_Sans_MS,
		padx = 12,
		command = lambda: openExpansionFrame(root, lessonFrame, lessonDetails, lessonPath)).pack(
		pady = 2,
		fill = 'x')

	backBtn = tk.Button(lessonFrame,
	text = 'Back',
	bg  = "#ff7735",
	font = font_Comic_Sans_MS,
	padx = 12,
	command = lambda: changeFrame(lessonFrame, lessonsListFrame)).pack(
	anchor = 'se',
	padx = 36,
	pady = 36,
	side = tk.BOTTOM)

def changeTranscriptFrame(nextTranscriptFrame):
	global currentTranscriptFrame
	changeFrame(currentTranscriptFrame, nextTranscriptFrame)
	currentTranscriptFrame = nextTranscriptFrame

def multilinetext(origtext, charlimit, strict_jap=False):
	# jap = True if strict_jap else (True if '\\u' in ascii(origtext) else False)
	jap = strict_jap
	wordlist = origtext if jap else origtext.split(" ")
	text = ""
	linecharcount = 0
	for i, word in enumerate(wordlist):
		linecharcount += len(word) if jap else len(word) + 1
		# joiner = 
		joiner = ("" if linecharcount < charlimit else "\n") if jap else ("" if i is 0 else " " if linecharcount < charlimit else "\n")
		if linecharcount >= charlimit:
			linecharcount = 0
		word = word.capitalize() if word.isupper() else word
		text += joiner + word
	return text

def openTranscriptFrame(root, lessonFrame, lessonDetails, lessonPath):
	transcriptFrame = tk.Frame(root, width=480, height=720)
	changeFrame(lessonFrame, transcriptFrame)
	scrollableFrame = ScrollFrame(transcriptFrame).frame

	menuFrame = tk.Frame(scrollableFrame)
	menuFrame.pack(fill='x')

	for i, transcript in enumerate(lessonDetails["data"]["Lesson"]["Transcript"]):
		langTranscript = tk.Frame(scrollableFrame)

		if i is 0:
			global currentTranscriptFrame
			currentTranscriptFrame = langTranscript
			currentTranscriptFrame.pack(fill="x")

		transcript_text = transcript["Lines"][1]["Text"]

		for line in transcript["Lines"]:
			characters = ['.', '?', '(', '!', '[']
			englishText = False
			for foo in characters:
				if foo in line["Text"]:
					englishText = True
					break
			if englishText:
				text = multilinetext(line["Text"], 55) 
			else:
				text = multilinetext(line["Text"], 24, strict_jap=True)

			voice_path = os.path.join(lessonPath, "Transcript", Path(line["Url"]).name)
			tk.Button(langTranscript,
			 text=f'{line["Name"]}\n{text}',
			 font=font_Comic_Sans_MS,
			 # height=3,
			 anchor="w",
			 justify="left",
			 width=41,
			 padx=6,
			 command=(lambda voicepath=voice_path: os.startfile(voicepath)) if os.path.getsize(voice_path)>1024 else ""
			 ).pack()

		tk.Button(menuFrame, 
			text=transcript["Heading"],
			bg  = "#76de3d",
			font = font_Comic_Sans_MS,
			padx = 15,
			command = lambda x=langTranscript: changeTranscriptFrame(x)).pack(
				pady = 4,
				padx = 2,
				side = 'left')

	backBtn = tk.Button(scrollableFrame,
		text = 'Back',
		bg  = "#ff7735",
		font = font_Comic_Sans_MS,
		padx = 12,
		command = lambda: changeFrame(transcriptFrame, lessonFrame)).pack(
			anchor = 'se',
			padx = 36,
			pady = 36,
			side = tk.BOTTOM)

def openExpansionFrame(root, lessonFrame, lessonDetails, lessonPath):
	expansionFrame = tk.Frame(root, width=480, height=720)
	changeFrame(lessonFrame, expansionFrame)
	scrollableFrame = ScrollFrame(expansionFrame).frame

	for item in lessonDetails["data"]["Lesson"]["Expansion"]:
		def_term = ["Definition", "Term"]
		for el in def_term:
			url = item[el]["Url"]
			url = os.path.join(lessonPath, Path(url).name)
			tk.Button(scrollableFrame,
				 text="Word: "+multilinetext(item[el]["Text"], 45),
				 font=font_Comic_Sans_MS,
				 bg = "#8cfffb",
				 # height=3,
				 anchor="w",
				 justify="left",
				 width=41,
				 padx=6,
				 command=(lambda url=url: os.startfile(url)) if os.path.getsize(url)>1024 else ""
				 ).pack(padx=4)

		for i, sample in enumerate(item["Samples"]):
			strict_jap = True if i == 0 else False
			url = sample["Url"] if sample.__contains__("Url") else url
			url = os.path.join(lessonPath, Path(url).name)
			tk.Button(scrollableFrame,
			 text=multilinetext(sample["Text"], 25 if strict_jap else 52, strict_jap=strict_jap),
			 font=font_Comic_Sans_MS,
			 bg = "#fff98c",
			 # height=3,
			 anchor="w",
			 justify="left",
			 width=41,
			 padx=6,
			 command=(lambda surl=url: os.startfile(surl)) if os.path.getsize(url)>1024 else ""
			 ).pack(padx=4)
		url = item["Term"]["Url"]
		url = os.path.join(lessonPath, Path(url).name)

	backBtn = tk.Button(scrollableFrame,
		text = 'Back',
		bg  = "#ff7735",
		font = font_Comic_Sans_MS,
		padx = 12,
		command = lambda: changeFrame(expansionFrame, lessonFrame)).pack(
			anchor = 'se',
			padx = 36,
			pady = 36,
			side = tk.BOTTOM)


def printDirectories(DirList):
	for _, directory in enumerate(DirList):
		print(_+1, directory)

def setCategory(currentDirList):
	# categoryNum = ['146', '153']
	category = {}
	
	for directory in currentDirList:
		catID = directory.split("-")[0]
		if not category.__contains__(catID):
			category[catID] = {}
			with open(os.path.join('.', 'lessons', directory, 'categoryInfo.txt'), 'r', encoding='utf-8') as fp:
				categoryTitle = fp.readline().strip('\n')
				category[catID]['title'] = categoryTitle
				print(categoryTitle)
			category[catID]['lessons'] = {}

		with open(os.path.join('.', 'lessons', directory, 'lessonDetails.txt'), 'r', encoding='utf-8') as fp:
			lessonDetails = json.load(fp)
			lessonDes = lessonDetails['data']['Lesson']['Title']
		category[catID]['lessons'][directory.split("-")[2]] = {}
		category[catID]['lessons'][directory.split("-")[2]]["Des"] = lessonDes				
		category[catID]['lessons'][directory.split("-")[2]]["Path"] = os.path.join(".", "lessons", directory)

	with open('currentDetails.txt', 'w', encoding='utf-8') as fp:
		# json.dump(category, fp,ensure_ascii=True, check_circular=True, allow_nan=True, indent=4)
		fp.write(json.dumps(category, ensure_ascii=False, indent=4))

def main():
	rootDir = "."
	rootDirs = [x for x in os.listdir('.') if not '.' in x]
	printDirectories(rootDirs)
	# print("\n")
	lessonsDir = os.path.join(rootDir, "lessons")
	# print(lessonsDir)
	currentDirList = [x for x in os.listdir(lessonsDir) if not '.' in x]
	# printDirectories(currentDirList)
	setCategory(currentDirList)
	# category = showCategory(currentDirList)
	# selectedCategory = selectCategory()
	# print(category[selectedCategory]["directories"])
	# printDirectories('.', rootDirs)
	UI()


if __name__ == '__main__':
	main()